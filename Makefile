check:
	pylint app.py
	pycodestyle app.py
	mypy app.py --ignore-missing-imports
build:
	docker build -t registry.gitlab.com/anjiproject/anji-webhook-endpoint:v0.2.0 .
push:
	docker push registry.gitlab.com/anjiproject/anji-webhook-endpoint:v0.2.0
test-run:
	docker run -it --rm --net host --name test-anji-webhook registry.gitlab.com/anjiproject/anji-webhook-endpoint:v0.2.0 sh