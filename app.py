import logging

from sanic import Sanic
from sanic.request import Request
from sanic.response import json, HTTPResponse
from sanic_prometheus import monitor
from sanic_sentry import SanicSentry

from anji_orm import register
from anji_core.types import InternalMessageType, InternalMessageState, NodeType
from anji_core.internal import InternalMessage

__author__ = "Bogdan Gladyshev"
__copyright__ = "Copyright 2017, Bogdan Gladyshev"
__credits__ = ["Bogdan Gladyshev"]
__license__ = "MIT"
__version__ = "0.2.0"
__maintainer__ = "Bogdan Gladyshev"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Production"

_log = logging.getLogger('sanic.base')

app = Sanic()
monitor(app, mmc_period_sec=None).expose_endpoint()


@app.listener('before_server_start')
async def sentry_check(sanic_app, _loop) -> None:
    if 'SENTRY_DSN' in sanic_app.config:
        SanicSentry().init_app(sanic_app)


@app.listener('before_server_start')
async def initial_configuration(sanic_app, _loop) -> None:
    sanic_app.config.setdefault('TOKEN', '12346')
    sanic_app.config.setdefault('BOT_IDENTIFIER', '@anji-tan')
    register.init(
        dict(
            host=sanic_app.config.get('RETHINKDB_HOST', '127.0.0.1'),
            port=sanic_app.config.get('RETHINKDB_PORT', 28015),
            db=sanic_app.config.get('RETHINKDB_DATABASE', 'test'),
            user=sanic_app.config.get('RETHINKDB_USER', 'admin'),
            password=sanic_app.config.get('RETHINKDB_PASSWORD', '')
        ),
        async_mode=True
    )
    await register.async_load()


@app.listener('after_server_stop')
async def close_connections(_sanic_app, _loop) -> None:
    await register.async_close()


@app.route('/execute')
async def send_phantom_command(request: Request) -> HTTPResponse:
    if 'token' in request.args and request.args.get('token') != request.app.config.TOKEN:
        return json({'message': 'invalid token'}, status=403)
    if 'command' not in request.args:
        return json({'message': 'command is required'}, status=400)
    message_record = InternalMessage(
        message=request.args.get('command'),
        state=InternalMessageState.posted,
        type=InternalMessageType.fake_message,
        source_user_technical_name=request.app.config.BOT_IDENTIFIER,
        target_indentifier=NodeType.master.name  # pylint: disable=no-member
    )
    await message_record.async_send()
    return json({'message': 'posted'})


if __name__ == "__main__":
    app.run(
        host=app.config.get('HOST', "0.0.0.0"),
        port=int(app.config.get('PORT', '9515'))
    )
