FROM alpine:3.7

# Install python

RUN apk add --no-cache python3 python3-dev py-pip gcc make musl-dev linux-headers libffi-dev openssl-dev && \
    pip3 install --upgrade pip && mkdir -p /app/anji-webhook/prometheus && pip3 install pipenv && cp /usr/bin/pip3 /usr/bin/pip

WORKDIR /app

ADD "Pipfile" "/app"
ADD "Pipfile.lock" "/app"

RUN /usr/bin/pipenv install --system --deploy

ADD "app.py" "/app"

EXPOSE 9515

ENV prometheus_multiproc_dir /app/anji-webhook/prometheus
ENV HOST 127.0.0.1
ENV PORT 9601

CMD ["gunicorn", "-w", "2", "-b", "${HOST}:${PORT}", "--capture-output", \
    "--worker-class", "sanic.worker.GunicornWorker", "--log-level", "INFO","app:app"]
