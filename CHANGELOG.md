# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.2.0]

### Added

- Sanic prometheus
- Ability to change port and host

### Changed

- Transformed to standalone application

### Fixed

- Use new anjiORM

[Unreleased]: https://gitlab.com/AnjiProject/anji-webhook-endpoint/compare/v0.2.0...HEAD
[0.2.0]: https://gitlab.com/AnjiProject/anji-webhook-endpoint/compare/v0.1.0...v0.2.0