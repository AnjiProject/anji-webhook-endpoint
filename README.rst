============================
AnjiProject Webhook endpoint
============================


.. image:: https://img.shields.io/pypi/v/anji-webhook.svg
        :target: https://pypi.python.org/pypi/anji_webhook

Webhook endpoint for AnjiProject


* Free software: MIT license


Features
--------

* Webhook to run commands
* Simple secret key filter

Configuration
-------------

You can configure this application via env variables

:SANIC_TOKEN: Token for webhook application
:SANIC_BOT_IDENTIFIER: Bot identifiers
:SANIC_RETHINKDB_HOST: RethinkDB host
:SANIC_RETHINKDB_PORT: RethinkDB port
:SANIC_RETHINKDB_DATABASE: RethinkDB database
:SANIC_RETHINKDB_USER: RethinkDB username
:SANIC_RETHINKDB_PASSWORD: RethinkDB password
:SANIC_SENTRY_DSN: Sentry dsn